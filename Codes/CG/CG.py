import numpy as np


def conjgrad(A, b, x):
    """
    A function to solve [A]{x} = {b} linear equation system with the
    conjugate gradient method.
    More at: http://en.wikipedia.org/wiki/Conjugate_gradient_method
    ========== Parameters ==========
    A : matrix
        A real symmetric positive definite matrix.
    b : vector
        The right hand side (RHS) vector of the system.
    x : vector
        The starting guess for the solution.
    """
    r = b - np.dot(A, x)
    p = r
    rsold = np.dot(np.transpose(r), r)

    while True:

        Ap = np.dot(A, p)

        alpha = rsold / np.dot(np.transpose(p), Ap)
        alpha = [[round(alpha[0][0], 4)]]
        print('-' * 100)
        x = x + alpha * p
        r = r - alpha * Ap
        rsnew = np.dot(np.transpose(r), r)
        if np.sqrt(rsnew) < 1e-10:
            break
        p = r + (rsnew / rsold) * p
        rsold = rsnew
        print(x)
    return x


a = [[5, 1, 1], [1, 5, 1], [1, 1, 5]]
b = [[7], [7], [7]]
x = [[0], [0], [0]]
cg = conjgrad(a, b, x)